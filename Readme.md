# Firefox [![pipeline status](https://gitlab.com/higenku/extapps/firefox/badges/dev/pipeline.svg)](https://gitlab.com/higenku/extapps/firefox/-/commits/dev)

Firefox port to GkRuntime. This repository shows how we would port the firefox app to the GkRuntime, you can build it from source or download the binary via our CI/CD